////
////  Loadable.swift
////  SkyVegasCompanion
////
////  Created by Jonathan Fulton on 06/11/2020.
////  Copyright © 2020 Sky Betting and Gaming. All rights reserved.
////
//
//import Foundation
//
//public enum LoadableState {
//    case notLoaded
//    case loading
//    case loaded
//    case failedToLoad
//}
//
//public enum LoadableError: Error {
//    case itemFailedToLoad([Error]?)
//}
//
//protocol Loadable {
//    var identifier: String { get set}
//    var state: LoadableState { get set }
//    var dependencies: Set<String> { get set }
//
//    func loadItem(completion: (Result<Void, Error>) -> Void)
//}
//
//class LoadableItem: Loadable {
//
//    public typealias LoadableAction = (_ completion: @escaping (Result<Void, Error>) -> Void) -> Void
//
//    var identifier: String
//    var state: LoadableState
//    var dependencies: Set<String>
//
//    var action: LoadableAction
//
//    func loadItem(completion: (Result<Void, Error>) -> Void) {}
//
//    init(itemIdentifier: String, itemDependencies: Set<String> = [], itemAction: @escaping LoadableAction) {
//        self.identifier = itemIdentifier
//        self.state = .notLoaded
//        self.dependencies = itemDependencies
//        self.action = itemAction
//    }
//}
//
//class LoadingManager {
//    private var loadItems: [Loadable]
//
//    private var completion: (Result<Void, LoadableError>) -> Void
//    private var loadedItems = Set<String>()
//    private var loadingError = [Error]()
//
//    init(items: [Loadable], completionHandler: @escaping (Result<Void, LoadableError>) -> Void) {
//        loadItems = items
//        completion = completionHandler
//    }
//
//    public func load() {
//        loadingError.removeAll()
//
//        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
//            guard let self = self else { return }
//            let group = DispatchGroup()
//
//            let items = self.loadItems.filter({ $0.state == .notLoaded && $0.dependencies.isSubset(of: self.loadedItems)})
//
//            if (items.isEmpty) {
//                self.completion(.success(()))
//                return
//            }
//
//            DispatchQueue.concurrentPerform(iterations: items.count) { index in
//                let item = items[index]
//                print("Entering group for \(item.identifier)")
//                group.enter()
//                item.loadItem() { result in
//                    switch result {
//                    case .success:
//                        print("- \(item.identifier) has loaded")
//                        self.loadedItems.insert(item.identifier)
//                    case .failure(let error):
//                        self.loadingError.append(error)
//                    }
//                    group.leave()
//                }
//            }
//
//            group.notify(queue: DispatchQueue.main) { [weak self] in
//                if self?.loadingError.isEmpty ?? true {
//                    self?.load()
//                } else {
//                    self?.completion(.failure(.itemFailedToLoad(self?.loadingError)))
//                }
//            }
//        }
//    }
//
//    public func restart() {
//        loadedItems.removeAll()
//        load()
//    }
//}
//
