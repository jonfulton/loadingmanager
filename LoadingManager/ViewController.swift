//
//  ViewController.swift
//  LoadingManager
//
//  Created by Jonathan Fulton on 14/08/2020.
//  Copyright © 2020 Jonathan Fulton. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let loadingManager = LoadingManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        let config = Config()
        let location = Location()
        let account = Account()
        let other = Other()

        loadingManager.register(loadItem: location)
        loadingManager.register(loadItem: config)
        loadingManager.register(loadItem: other)
        loadingManager.register(loadItem: account)

        loadingManager.startLoad()
    }
}

// --------- PROTOCOL / ENUMS ---------

protocol Loadable: AnyObject {
    var identifier: LoadableItemIdentifier { get set }
    var state: LoadableState { get set }
    var dependencies: Set<LoadableItemIdentifier> { get set }

    func loadItem(completion: (Result<String, Error>) -> ())
}

extension Loadable {
    func loadItem(completion: (Result<String, Error>) -> ()) {
        let sleepTime = UInt32.random(in: 1..<5)
        print("Loading \(identifier) which will take \(sleepTime) seconds...")
        sleep(sleepTime)
        state = .loaded
        completion(.success("Loading successful"))
    }
}

public enum LoadableState {
    case notLoaded
    case loading
    case loaded
    case failedToLoad
}

public enum LoadableItemIdentifier {
    case config
    case location
    case account
    case other
}

public enum LoadableError: Error {
    case genericError
}

// --------- LOADABLE ITEMS ---------

class Config: Loadable {
    var identifier = LoadableItemIdentifier.config
    var state = LoadableState.notLoaded
    var dependencies = Set<LoadableItemIdentifier>()
}

class Location: Loadable {
    var identifier = LoadableItemIdentifier.location
    var state = LoadableState.notLoaded
    var dependencies = Set<LoadableItemIdentifier>([LoadableItemIdentifier.config])

//    func loadItem(completion: (Result<String, Error>) -> ()) {
//        let sleepTime = UInt32.random(in: 1..<5)
//        print("Loading \(identifier) which will take \(sleepTime) seconds...")
//        sleep(sleepTime)
//        completion(.failure(LoadableError.genericError))
//    }
}

class Account: Loadable {
    var identifier = LoadableItemIdentifier.account
    var state = LoadableState.notLoaded
    var dependencies = Set<LoadableItemIdentifier>([LoadableItemIdentifier.config])
}

class Other: Loadable {
    var identifier = LoadableItemIdentifier.other
    var state = LoadableState.notLoaded
    var dependencies = Set<LoadableItemIdentifier>([LoadableItemIdentifier.config, LoadableItemIdentifier.account])
}

// --------- LOADING MANAGER ---------

class LoadingManager {
    var loadItems = [Loadable]()
    var loadedItems = Set<LoadableItemIdentifier>()

    var loadingError = [LoadableError]()

    public func register(loadItem: Loadable) {
        loadItems.append(loadItem)
    }

    public func startLoad() {
        // Display a loading screen
        loadingError.removeAll()
        load()
    }

    private func load() {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in

            guard let self = self else { return }
//        let queue = DispatchQueue(label: "LoadingManager", qos: .userInitiated, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil)
        let group = DispatchGroup()

            let items = self.loadItems.filter({ $0.state == .notLoaded && $0.dependencies.isSubset(of: self.loadedItems) })

        if items.isEmpty {
            self.loadingComplete()
            return
        }

        DispatchQueue.concurrentPerform(iterations: items.count) { index in
            let item = items[index]
            print("Entering group for \(item.identifier)")
            group.enter()
            item.loadItem() { result in
                switch result {
                case .success(let message):
                    print("- \(item.identifier): \(message)")
                    self.loadedItems.insert(item.identifier)
                case .failure(let error):
                    self.errorLoading(item: item, error: error as? LoadableError ?? LoadableError.genericError)
                }
                group.leave()
            }
        }


            group.notify(queue: DispatchQueue.main) { [weak self] in
            // Check for any errors and bail out if required.
            if self?.loadingError.isEmpty ?? true {
                self?.load()
            } else {
                self?.displayError()
            }

        }
        }
    }

    private func errorLoading(item: Loadable, error: LoadableError) {
        loadingError.append(error)
        print("- There was an error loading: \(item.identifier)")

    }

    private func loadingComplete() {
        print("All items loaded - continue about your business")
    }

    private func displayError() {
        print("An error was encountered, displaying error screen. Currently loaded items: \(loadedItems)")
    }
}
